<?php

namespace App\Http\Controllers;

require '../vendor/autoload.php';

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    public function handle(Request $form_data)
    {
        $this -> validate($form_data, [
            'user_name' => 'required',
            'user_email' => 'required|email',
            'callbackurl' => 'required'
        ]);

        $client = new Client([
            'base_uri' => 'https://somentetop.000webhostapp.com/',
        ]);

        $response = $client -> request('POST', 'callback', [
            'form_params' => [
                'user_name' => $form_data['user_name'],
                'user_email' => $form_data['user_email'],
                'callbackurl' => $form_data['callbackurl']
            ],
            'http_errors' => false,
        ]);

        return $response -> getBody();
    }
}